<?php
namespace fraek\src;

/**
 * First moment class to see if composer is working
 * Class TestClass
 * @package fraek\src
 */
class TestClass {

    /**
     * @return string
     */
    public function doWork() {
        return 'Did work';
    }
}